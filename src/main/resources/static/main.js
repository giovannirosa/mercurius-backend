(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/user/user.service */ "./src/app/services/user/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(userService) {
        this.userService = userService;
        this.showTemplate = false;
        this.shared = _services_shared_service__WEBPACK_IMPORTED_MODULE_1__["SharedService"].getInstance();
        // const now = moment();
        // console.log('hello world', now.format()); // add this 3 of 4
        // console.log(now.add(7, 'days').format()); // add this 4of 4
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.showTemplate.subscribe(function (show) { return _this.showTemplate = show; });
        //   this.shared.user = new User('','admin@mercurius.com','123456','');
        //   // this.userService.login(this.shared.user);
        //   this.userService.login(this.shared.user).subscribe((userAuthentication: CurrentUser) => {
        //     this.shared.token = userAuthentication.token;
        //     this.shared.user = userAuthentication.user;
        //     this.shared.user.profile = this.shared.user.profile.substring(5);
        //     // this.shared.showTemplate.emit(true);
        //     // this.router.navigate(['/']);
        // } , err => {
        //   this.shared.token = null;
        //   this.shared.user = null;
        //   // this.shared.showTemplate.emit(false);
        //   // this.message = 'Erro ';
        // });
    };
    AppComponent.prototype.showContentWrapper = function () {
        return {
            'content-wrapper': this.shared.isLoggedIn()
        };
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_services_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: momentAdapterFactory, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentAdapterFactory", function() { return momentAdapterFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _pipes_customer_pipe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pipes/customer.pipe */ "./src/app/pipes/customer.pipe.ts");
/* harmony import */ var _components_customer_table_customer_table_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/customer-table/customer-table.component */ "./src/app/components/customer-table/customer-table.component.ts");
/* harmony import */ var _components_security_auth_interceptor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/security/auth.interceptor */ "./src/app/components/security/auth.interceptor.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _components_security_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/security/auth.guard */ "./src/app/components/security/auth.guard.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _components_security_login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/security/login/login.component */ "./src/app/components/security/login/login.component.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/navigation/navigation.component */ "./src/app/components/navigation/navigation.component.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_customer_form_customer_form_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/customer-form/customer-form.component */ "./src/app/components/customer-form/customer-form.component.ts");
/* harmony import */ var _material__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./material */ "./src/app/material.ts");
/* harmony import */ var _components_graphic_graphic_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/graphic/graphic.component */ "./src/app/components/graphic/graphic.component.ts");
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./app.routes */ "./src/app/app.routes.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/common/locales/pt */ "./node_modules/@angular/common/locales/pt.js");
/* harmony import */ var _angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material-moment-adapter */ "./node_modules/@angular/material-moment-adapter/esm5/material-moment-adapter.es5.js");
/* harmony import */ var _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/confirmation-dialog/confirmation-dialog.component */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts");
/* harmony import */ var _angular_service_worker__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/service-worker */ "./node_modules/@angular/service-worker/fesm5/service-worker.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _components_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/agenda/agenda.component */ "./src/app/components/agenda/agenda.component.ts");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm5/angular-calendar.js");
/* harmony import */ var angular_calendar_date_adapters_moment__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! angular-calendar/date-adapters/moment */ "./node_modules/angular-calendar/date-adapters/moment/index.js");
/* harmony import */ var angular_calendar_date_adapters_moment__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(angular_calendar_date_adapters_moment__WEBPACK_IMPORTED_MODULE_30__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_31__);
/* harmony import */ var _components_user_info_user_info_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/user-info/user-info.component */ "./src/app/components/user-info/user-info.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























// import { ResponsiveColsDirective } from './directives/responsive-cols.directive';







Object(_angular_common__WEBPACK_IMPORTED_MODULE_22__["registerLocaleData"])(_angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_23___default.a, 'pt-BR');
function momentAdapterFactory() {
    return Object(angular_calendar_date_adapters_moment__WEBPACK_IMPORTED_MODULE_30__["adapterFactory"])(moment__WEBPACK_IMPORTED_MODULE_31__);
}
var MY_FORMATS = {
    parse: {
        dateInput: 'YYYY-MM-DD',
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'YYYY-MM-DD',
        monthYearA11yLabel: 'MMMM YYYY'
    },
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_8__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_11__["NavigationComponent"],
                _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_13__["DashboardComponent"],
                _components_customer_form_customer_form_component__WEBPACK_IMPORTED_MODULE_14__["CustomerFormComponent"],
                _components_graphic_graphic_component__WEBPACK_IMPORTED_MODULE_16__["GraphicComponent"],
                _components_security_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
                _components_customer_table_customer_table_component__WEBPACK_IMPORTED_MODULE_1__["CustomerTableComponent"],
                _pipes_customer_pipe__WEBPACK_IMPORTED_MODULE_0__["CustomerPipe"],
                _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_25__["ConfirmationDialogComponent"],
                // ResponsiveColsDirective,
                _components_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_28__["AgendaComponent"],
                _components_user_info_user_info_component__WEBPACK_IMPORTED_MODULE_32__["UserInfoComponent"]
            ],
            entryComponents: [
                _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_25__["ConfirmationDialogComponent"],
                _components_customer_form_customer_form_component__WEBPACK_IMPORTED_MODULE_14__["CustomerFormComponent"],
                _components_user_info_user_info_component__WEBPACK_IMPORTED_MODULE_32__["UserInfoComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"],
                _material__WEBPACK_IMPORTED_MODULE_15__["MaterialModule"],
                _app_routes__WEBPACK_IMPORTED_MODULE_17__["routes"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_18__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_19__["FormsModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_12__["FlexLayoutModule"],
                _angular_service_worker__WEBPACK_IMPORTED_MODULE_26__["ServiceWorkerModule"].register('ngsw-worker.js', { enabled: _environments_environment__WEBPACK_IMPORTED_MODULE_27__["environment"].production }),
                angular_calendar__WEBPACK_IMPORTED_MODULE_29__["CalendarModule"].forRoot({
                    provide: angular_calendar__WEBPACK_IMPORTED_MODULE_29__["DateAdapter"],
                    useFactory: momentAdapterFactory
                }, {
                    dateFormatter: {
                        provide: angular_calendar__WEBPACK_IMPORTED_MODULE_29__["CalendarDateFormatter"],
                        useClass: angular_calendar__WEBPACK_IMPORTED_MODULE_29__["CalendarMomentDateFormatter"]
                    }
                })
            ],
            providers: [
                _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
                _components_security_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"],
                _services_shared_service__WEBPACK_IMPORTED_MODULE_3__["SharedService"],
                _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_20__["CustomerService"],
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_18__["HTTP_INTERCEPTORS"],
                    useClass: _components_security_auth_interceptor__WEBPACK_IMPORTED_MODULE_2__["AuthInterceptor"],
                    multi: true
                },
                { provide: _angular_core__WEBPACK_IMPORTED_MODULE_8__["LOCALE_ID"],
                    useValue: 'pt-BR'
                },
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_21__["DateAdapter"],
                    useClass: _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_24__["MomentDateAdapter"],
                    deps: [_angular_material__WEBPACK_IMPORTED_MODULE_21__["MAT_DATE_LOCALE"]]
                },
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_21__["MAT_DATE_FORMATS"],
                    useValue: MY_FORMATS
                },
                { provide: angular_calendar__WEBPACK_IMPORTED_MODULE_29__["MOMENT"],
                    useValue: moment__WEBPACK_IMPORTED_MODULE_31__
                },
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_21__["MAT_SNACK_BAR_DEFAULT_OPTIONS"],
                    useValue: { duration: 5000 }
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routes.ts":
/*!*******************************!*\
  !*** ./src/app/app.routes.ts ***!
  \*******************************/
/*! exports provided: ROUTES, routes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony import */ var _components_customer_table_customer_table_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/customer-table/customer-table.component */ "./src/app/components/customer-table/customer-table.component.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_security_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/security/login/login.component */ "./src/app/components/security/login/login.component.ts");
/* harmony import */ var _components_security_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/security/auth.guard */ "./src/app/components/security/auth.guard.ts");
/* harmony import */ var _components_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/agenda/agenda.component */ "./src/app/components/agenda/agenda.component.ts");






var ROUTES = [
    { path: 'login', component: _components_security_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: '', component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_1__["DashboardComponent"], canActivate: [_components_security_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
    //{ path: 'clientes-form' , component: CustomerFormComponent, canActivate: [AuthGuard] },
    { path: 'clientes', component: _components_customer_table_customer_table_component__WEBPACK_IMPORTED_MODULE_0__["CustomerTableComponent"], canActivate: [_components_security_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
    { path: 'agenda', component: _components_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_5__["AgendaComponent"], canActivate: [_components_security_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] }
];
var routes = _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(ROUTES);


/***/ }),

/***/ "./src/app/components/agenda/agenda.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/agenda/agenda.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cal-body {\n  margin: 0.5em;\n}\n\n.title {\n  /*border: 1px solid black;*/\n  display: flex;\n  margin: 1em;\n  text-transform: capitalize;\n}\n\n.button-group {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n}\n\n.header {\n  /*border: 1px solid black;*/\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n  align-items: center;\n  /*align-content: center;*/\n  margin: 0 0.5em 0.5em 0.5em;\n}\n\nbody {\n  font-family: 'Roboto Sans';\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZ2VuZGEvYWdlbmRhLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSwyQkFBMkI7RUFDM0IsYUFBYTtFQUNiLFdBQVc7RUFDWCwwQkFBMEI7QUFDNUI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGVBQWU7QUFDakI7O0FBRUE7RUFDRSwyQkFBMkI7RUFDM0IsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsOEJBQThCO0VBQzlCLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsMkJBQTJCO0FBQzdCOztBQUVBO0VBQ0UsMEJBQTBCO0FBQzVCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZ2VuZGEvYWdlbmRhLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FsLWJvZHkge1xuICBtYXJnaW46IDAuNWVtO1xufVxuXG4udGl0bGUge1xuICAvKmJvcmRlcjogMXB4IHNvbGlkIGJsYWNrOyovXG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbjogMWVtO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLmJ1dHRvbi1ncm91cCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmhlYWRlciB7XG4gIC8qYm9yZGVyOiAxcHggc29saWQgYmxhY2s7Ki9cbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgLyphbGlnbi1jb250ZW50OiBjZW50ZXI7Ki9cbiAgbWFyZ2luOiAwIDAuNWVtIDAuNWVtIDAuNWVtO1xufVxuXG5ib2R5IHtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8gU2Fucyc7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/agenda/agenda.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/agenda/agenda.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"cal-body mat-elevation-z8\">\n  <div class=\"header\">\n    <div class=\"button-group\">\n      <button\n        mat-raised-button color=\"primary\"\n        mwlCalendarPreviousView\n        [view]=\"view\"\n        [(viewDate)]=\"viewDate\"\n        (viewDateChange)=\"activeDayIsOpen = false\">\n        Anterior\n      </button>\n      <button\n        mat-raised-button\n        mwlCalendarToday\n        [(viewDate)]=\"viewDate\">\n        Hoje\n      </button>\n      <button\n        mat-raised-button color=\"primary\"\n        mwlCalendarNextView\n        [view]=\"view\"\n        [(viewDate)]=\"viewDate\"\n        (viewDateChange)=\"activeDayIsOpen = false\">\n        Próximo\n      </button>\n    </div>\n    <span class=\"mat-headline title\">{{ viewDate | calendarDate:(view + 'ViewTitle') }}</span>\n    <div>\n      <button\n        mat-raised-button color=\"primary\"\n        (click)=\"view = CalendarView.Month\"\n        [class.active]=\"view === CalendarView.Month\">\n        Mês\n      </button>\n      <button\n        mat-raised-button color=\"primary\"\n        (click)=\"view = CalendarView.Week\"\n        [class.active]=\"view === CalendarView.Week\">\n        Semana\n      </button>\n      <button\n        mat-raised-button color=\"primary\"\n        (click)=\"view = CalendarView.Day\"\n        [class.active]=\"view === CalendarView.Day\">\n        Dia\n      </button>\n    </div>\n  </div>\n  <div [ngSwitch]=\"view\" class=\"mat-body\">\n    <mwl-calendar-month-view\n      *ngSwitchCase=\"'month'\"\n      [viewDate]=\"viewDate\"\n      [events]=\"events\"\n      (eventClicked)=\"handleEvent('Clicked', $event.event)\"\n      (dayClicked)=\"handleEvent('Clicked', $event.day.date)\"\n      (eventTimesChanged)=\"eventTimesChanged($event)\">\n    </mwl-calendar-month-view>\n    <mwl-calendar-week-view\n      *ngSwitchCase=\"'week'\"\n      [viewDate]=\"viewDate\"\n      [events]=\"events\"\n      (eventClicked)=\"handleEvent('Clicked', $event.event)\"\n      (eventTimesChanged)=\"eventTimesChanged($event)\">\n    </mwl-calendar-week-view>\n    <mwl-calendar-day-view\n      *ngSwitchCase=\"'day'\"\n      [viewDate]=\"viewDate\"\n      [events]=\"events\"\n      (eventClicked)=\"handleEvent('Clicked', $event.event)\"\n      (eventTimesChanged)=\"eventTimesChanged($event)\">\n    </mwl-calendar-day-view>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/agenda/agenda.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/agenda/agenda.component.ts ***!
  \*******************************************************/
/*! exports provided: AgendaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaComponent", function() { return AgendaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm5/angular-calendar.js");
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! date-fns */ "./node_modules/date-fns/index.js");
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// weekStartsOn option is ignored when using moment, as it needs to be configured globally for the moment locale
moment__WEBPACK_IMPORTED_MODULE_3__["updateLocale"]('en', {
    week: {
        dow: angular_calendar__WEBPACK_IMPORTED_MODULE_1__["DAYS_OF_WEEK"].MONDAY,
        doy: 0
    }
});
var colors = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};
var AgendaComponent = /** @class */ (function () {
    function AgendaComponent() {
        var _this = this;
        this.view = angular_calendar__WEBPACK_IMPORTED_MODULE_1__["CalendarView"].Month;
        this.CalendarView = angular_calendar__WEBPACK_IMPORTED_MODULE_1__["CalendarView"];
        this.viewDate = new Date();
        this.refresh = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.actions = [
            {
                label: '<i class="fa fa-fw fa-pencil"></i>',
                onClick: function (_a) {
                    var event = _a.event;
                    _this.handleEvent('Edited', event);
                }
            },
            {
                label: '<i class="fa fa-fw fa-times"></i>',
                onClick: function (_a) {
                    var event = _a.event;
                    _this.events = _this.events.filter(function (iEvent) { return iEvent !== event; });
                    _this.handleEvent('Deleted', event);
                }
            }
        ];
        this.events = [
            {
                start: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["subDays"])(Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["startOfDay"])(new Date()), 1),
                end: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["addDays"])(new Date(), 1),
                title: 'A 3 day event',
                color: colors.red,
                actions: this.actions,
                allDay: true,
                resizable: {
                    beforeStart: true,
                    afterEnd: true
                },
                draggable: true
            },
            {
                start: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["startOfDay"])(new Date()),
                title: 'An event with no end date',
                color: colors.yellow,
                actions: this.actions
            },
            {
                start: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["subDays"])(Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["endOfMonth"])(new Date()), 3),
                end: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["addDays"])(Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["endOfMonth"])(new Date()), 3),
                title: 'A long event that spans 2 months',
                color: colors.blue,
                allDay: true
            },
            {
                start: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["addHours"])(Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["startOfDay"])(new Date()), 2),
                end: new Date(),
                title: 'A draggable and resizable event',
                color: colors.yellow,
                actions: this.actions,
                resizable: {
                    beforeStart: true,
                    afterEnd: true
                },
                draggable: true
            }
        ];
        this.activeDayIsOpen = true;
    }
    // constructor(private modal: NgbModal) {}
    AgendaComponent.prototype.dayClicked = function (_a) {
        var date = _a.date, events = _a.events;
        if (Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["isSameMonth"])(date, this.viewDate)) {
            this.viewDate = date;
            if ((Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["isSameDay"])(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0) {
                this.activeDayIsOpen = false;
            }
            else {
                this.activeDayIsOpen = true;
            }
        }
    };
    AgendaComponent.prototype.eventTimesChanged = function (_a) {
        var event = _a.event, newStart = _a.newStart, newEnd = _a.newEnd;
        event.start = newStart;
        event.end = newEnd;
        this.handleEvent('Dropped or resized', event);
        this.refresh.next();
        console.log('Time Changed');
    };
    AgendaComponent.prototype.handleEvent = function (action, event) {
        // this.modalData = { event, action };
        // this.modal.open(this.modalContent, { size: 'lg' });
        console.log('CLICK', event);
    };
    AgendaComponent.prototype.addEvent = function () {
        this.events.push({
            title: 'New event',
            start: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["startOfDay"])(new Date()),
            end: Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["endOfDay"])(new Date()),
            color: colors.red,
            draggable: true,
            resizable: {
                beforeStart: true,
                afterEnd: true
            }
        });
        this.refresh.next();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('modalContent'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"])
    ], AgendaComponent.prototype, "modalContent", void 0);
    AgendaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-agenda',
            template: __webpack_require__(/*! ./agenda.component.html */ "./src/app/components/agenda/agenda.component.html"),
            styles: [__webpack_require__(/*! ./agenda.component.css */ "./src/app/components/agenda/agenda.component.css")]
        })
    ], AgendaComponent);
    return AgendaComponent;
}());



/***/ }),

/***/ "./src/app/components/confirmation-dialog/confirmation-dialog.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/components/confirmation-dialog/confirmation-dialog.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLWRpYWxvZy9jb25maXJtYXRpb24tZGlhbG9nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/confirmation-dialog/confirmation-dialog.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/confirmation-dialog/confirmation-dialog.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3 mat-dialog-title>{{message}}</h3>\n<!-- <mat-dialog-content>{{message}}</mat-dialog-content> -->\n<mat-dialog-actions fxLayout=\"row\" fxLayoutAlign=\"flex-end\">\n  <button mat-raised-button color=\"primary\" mat-dialog-close>Não</button>\n  <!-- The mat-dialog-close directive optionally accepts a value as a result for the dialog. -->\n  <button mat-raised-button color=\"warn\" [mat-dialog-close]=\"true\">Sim</button>\n</mat-dialog-actions>\n"

/***/ }),

/***/ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/confirmation-dialog/confirmation-dialog.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ConfirmationDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationDialogComponent", function() { return ConfirmationDialogComponent; });
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConfirmationDialogComponent = /** @class */ (function () {
    function ConfirmationDialogComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ConfirmationDialogComponent.prototype.ngOnInit = function () {
    };
    ConfirmationDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirmation-dialog',
            template: __webpack_require__(/*! ./confirmation-dialog.component.html */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.html"),
            styles: [__webpack_require__(/*! ./confirmation-dialog.component.css */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialogRef"]])
    ], ConfirmationDialogComponent);
    return ConfirmationDialogComponent;
}());



/***/ }),

/***/ "./src/app/components/customer-form/customer-form.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/components/customer-form/customer-form.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}\n\nmat-form-field {\n  width: 100%;\n}\n\n.half {\n  width: 50%;\n}\n\n.center {\n  text-align: center;\n  /* margin: auto;\n  width: 100%; */\n  /* border: 3px solid green; */\n  padding: 0.5em;\n}\n\n.center button {\n  margin: 0.5em;\n}\n\n.right {\n  text-align: right;\n  /* position: absolute;\n  right: 0px;\n  padding: 10px; */\n}\n\n.left {\n  text-align: left;\n  /* position: absolute;\n  left: 0px;\n  padding: 10px; */\n}\n\n.fill-space {\n  /* This fills the remaining space, by using flexbox.\n   Every toolbar row uses a flexbox row layout.*/\n  flex: 1 1 auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jdXN0b21lci1mb3JtL2N1c3RvbWVyLWZvcm0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEI7Z0JBQ2M7RUFDZCw2QkFBNkI7RUFDN0IsY0FBYztBQUNoQjs7QUFFQTtFQUNFLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQjs7a0JBRWdCO0FBQ2xCOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCOztrQkFFZ0I7QUFDbEI7O0FBRUE7RUFDRTtnREFDOEM7RUFDOUMsY0FBYztBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY3VzdG9tZXItZm9ybS9jdXN0b21lci1mb3JtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIgPiAqIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5oYWxmIHtcbiAgd2lkdGg6IDUwJTtcbn1cblxuLmNlbnRlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgLyogbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogMTAwJTsgKi9cbiAgLyogYm9yZGVyOiAzcHggc29saWQgZ3JlZW47ICovXG4gIHBhZGRpbmc6IDAuNWVtO1xufVxuXG4uY2VudGVyIGJ1dHRvbiB7XG4gIG1hcmdpbjogMC41ZW07XG59XG5cbi5yaWdodCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICAvKiBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwcHg7XG4gIHBhZGRpbmc6IDEwcHg7ICovXG59XG5cbi5sZWZ0IHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgLyogcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwcHg7XG4gIHBhZGRpbmc6IDEwcHg7ICovXG59XG5cbi5maWxsLXNwYWNlIHtcbiAgLyogVGhpcyBmaWxscyB0aGUgcmVtYWluaW5nIHNwYWNlLCBieSB1c2luZyBmbGV4Ym94LlxuICAgRXZlcnkgdG9vbGJhciByb3cgdXNlcyBhIGZsZXhib3ggcm93IGxheW91dC4qL1xuICBmbGV4OiAxIDEgYXV0bztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/customer-form/customer-form.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/customer-form/customer-form.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-dialog-content>\n  <form class=\"form-vertical\" #form=\"ngForm\">\n    <mat-form-field class=\"example-full-width\">\n      <mat-icon matPrefix>person</mat-icon>\n      <input\n        matInput\n        type=\"text\"\n        [(ngModel)]=\"customer.name\"\n        name=\"name\"\n        class=\"form-control\"\n        id=\"inputName\"\n        placeholder=\"Nome\"\n        #name=\"ngModel\"\n        required\n      />\n    </mat-form-field>\n\n    <mat-form-field>\n      <mat-icon matPrefix>star</mat-icon>\n      <mat-select\n        placeholder=\"Reputação\"\n        [(ngModel)]=\"customer.reputation\"\n        class=\"form-control select2\"\n        name=\"reputation\"\n        id=\"inputReputation\"\n        #reputation=\"ngModel\"\n        style=\"width: 100%;\"\n        required\n      >\n        <mat-option value=\"Boa\">Boa</mat-option>\n        <mat-option value=\"Média\">Média</mat-option>\n        <mat-option value=\"Ruim\">Ruim</mat-option>\n      </mat-select>\n    </mat-form-field>\n\n    <mat-form-field>\n      <mat-icon matPrefix>supervisor_account</mat-icon>\n      <mat-select\n        placeholder=\"Sexo\"\n        [(ngModel)]=\"customer.gender\"\n        class=\"form-control select2\"\n        name=\"gender\"\n        id=\"inputGender\"\n        #gender=\"ngModel\"\n        style=\"width: 100%;\"\n        required\n      >\n        <mat-option value=\"Masculino\">Masculino</mat-option>\n        <mat-option value=\"Feminino\">Feminino</mat-option>\n      </mat-select>\n    </mat-form-field>\n\n    <mat-form-field class=\"half\">\n      <mat-icon matPrefix>smartphone</mat-icon>\n      <input\n        matInput\n        [(ngModel)]=\"customer.telephone[0]\"\n        name=\"mobile\"\n        class=\"form-control\"\n        id=\"inputMobile\"\n        type=\"text\"\n        placeholder=\"Celular\"\n        #mobile=\"ngModel\"\n        required\n      />\n    </mat-form-field>\n\n    <mat-form-field class=\"half\">\n      <mat-icon matPrefix>local_phone</mat-icon>\n      <input\n        matInput\n        [(ngModel)]=\"customer.telephone[1]\"\n        name=\"telephone\"\n        class=\"form-control\"\n        id=\"inputTelephone\"\n        type=\"text\"\n        placeholder=\"Telefone\"\n        #telephone=\"ngModel\"\n      />\n    </mat-form-field>\n\n    <mat-form-field>\n      <mat-icon matPrefix>email</mat-icon>\n      <input\n        matInput\n        [(ngModel)]=\"customer.email[0]\"\n        name=\"email\"\n        class=\"form-control\"\n        id=\"inputEmail\"\n        type=\"text\"\n        placeholder=\"Email\"\n        #email=\"ngModel\"\n      />\n    </mat-form-field>\n\n    <mat-form-field>\n      <mat-icon matPrefix>cake</mat-icon>\n      <input\n        matInput\n        [matDatepicker]=\"picker\"\n        placeholder=\"Aniversário\"\n        [ngModel]=\"customer.birthday | date: 'yyyy-MM-dd'\"\n        (ngModelChange)=\"customer.birthday = $event\"\n        name=\"birthday\"\n        class=\"form-control pull-right\"\n        id=\"inputBirthday\"\n        type=\"date\"\n        #birthday=\"ngModel\"\n      />\n      <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n      <mat-datepicker touchUi #picker></mat-datepicker>\n    </mat-form-field>\n\n    <mat-form-field>\n      <mat-icon matPrefix>assignment_ind</mat-icon>\n      <input\n        matInput\n        placeholder=\"CPF\"\n        [(ngModel)]=\"customer.cpf\"\n        name=\"cpf\"\n        class=\"form-control\"\n        id=\"inputCpf\"\n        type=\"text\"\n        #cpf=\"ngModel\"\n      />\n    </mat-form-field>\n\n    <mat-form-field>\n      <mat-icon matPrefix>cached</mat-icon>\n      <input\n        matInput\n        placeholder=\"Periodicidade\"\n        [(ngModel)]=\"customer.period\"\n        name=\"period\"\n        class=\"form-control\"\n        id=\"inputPeriod\"\n        type=\"number\"\n        min=\"0\"\n        max=\"100\"\n        #period=\"ngModel\"\n      />\n    </mat-form-field>\n\n    <mat-form-field>\n      <mat-icon matPrefix>insert_comment</mat-icon>\n      <textarea\n        matInput\n        placeholder=\"Notas\"\n        [(ngModel)]=\"customer.notes\"\n        name=\"notes\"\n        class=\"form-control\"\n        id=\"inputNotes\"\n        type=\"text\"\n        placeholder=\"Comentários\"\n        #notes=\"ngModel\"\n        maxlength=\"800\"\n      ></textarea>\n    </mat-form-field>\n  </form>\n</mat-dialog-content>\n<mat-dialog-actions fxLayout=\"row\" fxLayoutAlign=\"flex-end\">\n  <button mat-raised-button color=\"warn\" (click)=\"close()\">\n    {{ data.close }}\n  </button>\n  <button mat-raised-button color=\"primary\" (click)=\"register()\">\n    {{ data.ok }}\n  </button>\n</mat-dialog-actions>\n"

/***/ }),

/***/ "./src/app/components/customer-form/customer-form.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/customer-form/customer-form.component.ts ***!
  \*********************************************************************/
/*! exports provided: CustomerFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerFormComponent", function() { return CustomerFormComponent; });
/* harmony import */ var _confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../confirmation-dialog/confirmation-dialog.component */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _model_customer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/customer */ "./src/app/model/customer.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var CustomerFormComponent = /** @class */ (function () {
    function CustomerFormComponent(customerService, dialogRef, data, dialog) {
        this.customerService = customerService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.dialog = dialog;
        this.shared = _services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"].getInstance();
        this.customer = data.customer;
        var telCopy = [];
        var i = 0;
        this.customer.telephone.forEach(function (element) {
            telCopy[i++] = element;
        });
        var emailCopy = [];
        i = 0;
        this.customer.email.forEach(function (element) {
            emailCopy[i++] = element;
        });
        this.original = new _model_customer__WEBPACK_IMPORTED_MODULE_3__["Customer"](this.customer.id, this.customer.name, this.customer.reputation, this.customer.gender, telCopy, emailCopy, this.customer.birthday, this.customer.cpf, this.customer.period, this.customer.notes, this.customer.dateEntered);
        dialogRef.disableClose = true;
    }
    CustomerFormComponent.prototype.ngOnInit = function () {
        // this.dialogRef.updateSize('80%', '80%');
    };
    CustomerFormComponent.prototype.close = function () {
        var _this = this;
        // console.log(JSON.stringify(this.original));
        // console.log(JSON.stringify(this.customer));
        if (JSON.stringify(this.original) === JSON.stringify(this.customer)) {
            this.dialogRef.close();
        }
        else {
            var confirmDialog = this.dialog.open(_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_0__["ConfirmationDialogComponent"]);
            confirmDialog.componentInstance.message = 'Existem alterações que não foram salvas. Deseja mesmo fechar?';
            confirmDialog.afterClosed().subscribe(function (result) {
                console.log(result);
                if (result) {
                    _this.dialogRef.close();
                }
            });
        }
    };
    CustomerFormComponent.prototype.findById = function (id) {
        var _this = this;
        this.customerService.findById(id).subscribe(function (responseApi) {
            _this.customer = responseApi.data;
        }, function (err) {
            _this.showMessage({
                type: 'error',
                text: err['error']['errors'][0]
            });
        });
    };
    CustomerFormComponent.prototype.register = function () {
        var _this = this;
        this.message = {};
        if (this.customer.name === '' ||
            this.customer.reputation === '' ||
            this.customer.gender === '' ||
            this.customer.telephone[0] === '') {
            return;
        }
        this.customerService.createOrUpdate(this.customer).subscribe(function (responseApi) {
            _this.customer = _this.shared.newCustomer();
            var customer = responseApi.data;
            _this.form.resetForm();
            _this.dialogRef.close();
            _this.showMessage({
                type: 'success',
                text: "Registered " + customer.name + " successfully"
            });
        }, function (err) {
            _this.showMessage({
                type: 'error',
                text: err['error']['errors'][0]
            });
        });
    };
    CustomerFormComponent.prototype.getFormGroupClass = function (model) {
        return {
            'form-group': true,
            'has-error': !model.valid && (model.dirty),
            'has-success': model.valid && (model.dirty)
        };
    };
    CustomerFormComponent.prototype.showMessage = function (message) {
        var _this = this;
        this.message = message;
        this.buildClasses(message.type);
        setTimeout(function () {
            _this.message = undefined;
        }, 3000);
    };
    CustomerFormComponent.prototype.buildClasses = function (type) {
        this.classCss = {
            'alert': true
        };
        this.classCss['alert-' + type] = true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"])('form'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"])
    ], CustomerFormComponent.prototype, "form", void 0);
    CustomerFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-customer-form',
            template: __webpack_require__(/*! ./customer-form.component.html */ "./src/app/components/customer-form/customer-form.component.html"),
            styles: [__webpack_require__(/*! ./customer-form.component.css */ "./src/app/components/customer-form/customer-form.component.css")]
        }),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_6__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_services_customer_customer_service__WEBPACK_IMPORTED_MODULE_1__["CustomerService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialogRef"], Object, _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialog"]])
    ], CustomerFormComponent);
    return CustomerFormComponent;
}());



/***/ }),

/***/ "./src/app/components/customer-table/customer-table.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/customer-table/customer-table.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Structure */\n/* table {\n    width: 100%;\n} */\n.example-header {\n    width: 100%;\n    text-align: left;\n}\n.right {\n    position: absolute;\n    right: 0px;\n    padding: 10px;\n}\n.outer {\n    padding: 0.5em;\n}\nbody {\n    font-family: 'Covered By Your Grace', cursive;\n    line-height: 1.25;\n}\n.fill {\n    width: 0.5em;\n}\n.actions {\n    display:flex !important;\n    justify-content:flex-end!important;\n}\n@media screen and (max-width: 960px) {\n    .mat-table {\n      border: 0;\n      vertical-align: middle;\n    }\n  \n    .mat-table caption {\n      font-size: 1em;\n    }\n    \n    /*  Enable this to hide header*/\n    .mat-table .mat-header-cell {\n      \n      border: 10px solid;\n      clip: rect(0 0 0 0);\n      height: 1px;\n      margin: -1px;\n      padding: 0;\n      position: absolute;\n      width: 1px;\n    }\n    \n    .mat-table .mat-row {\n      border-bottom: 5px solid #ddd;\n      display: block;\n      /* height: 280px; */\n      /* border: 2px solid red; */\n    }\n    /*\n    .mat-table .mat-row:nth-child(even) {background: #CCC}\n    .mat-table .mat-row:nth-child(odd) {background: #FFF}\n    */\n    .mat-table .mat-cell {\n      border-bottom: 1px solid #ddd;\n      display: block;\n      font-size: 1em;\n      text-align: right;\n      font-weight: bold;\n      height:30px;\n      margin-bottom: 4%;\n      padding-right: 2.0em;\n      padding-left: 2.0em;\n      /* border: 2px solid green; */\n    }\n    .mat-table .mat-cell:before {\n      /*\n      * aria-label has no advantage, it won't be read inside a table\n      content: attr(aria-label);\n      */\n      content: attr(data-label);\n      float: left;\n      text-transform: uppercase;\n      font-weight: normal;\n      \n      font-size: .85em;\n    }\n    .mat-table .mat-cell:last-child {\n      border-bottom: 0;\n    }\n    .mat-table .mat-cell:first-child {\n      margin-top: 4%;\n    }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jdXN0b21lci10YWJsZS9jdXN0b21lci10YWJsZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGNBQWM7QUFDZDs7R0FFRztBQUNIO0lBQ0ksV0FBVztJQUNYLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixhQUFhO0FBQ2pCO0FBRUE7SUFDSSxjQUFjO0FBQ2xCO0FBRUE7SUFDSSw2Q0FBNkM7SUFDN0MsaUJBQWlCO0FBQ3JCO0FBRUE7SUFDSSxZQUFZO0FBQ2hCO0FBRUE7SUFDSSx1QkFBdUI7SUFDdkIsa0NBQWtDO0FBQ3RDO0FBRUE7SUFDSTtNQUNFLFNBQVM7TUFDVCxzQkFBc0I7SUFDeEI7O0lBRUE7TUFDRSxjQUFjO0lBQ2hCOztJQUVBLCtCQUErQjtJQUMvQjs7TUFFRSxrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLFdBQVc7TUFDWCxZQUFZO01BQ1osVUFBVTtNQUNWLGtCQUFrQjtNQUNsQixVQUFVO0lBQ1o7O0lBRUE7TUFDRSw2QkFBNkI7TUFDN0IsY0FBYztNQUNkLG1CQUFtQjtNQUNuQiwyQkFBMkI7SUFDN0I7SUFDQTs7O0tBR0M7SUFDRDtNQUNFLDZCQUE2QjtNQUM3QixjQUFjO01BQ2QsY0FBYztNQUNkLGlCQUFpQjtNQUNqQixpQkFBaUI7TUFDakIsV0FBVztNQUNYLGlCQUFpQjtNQUNqQixvQkFBb0I7TUFDcEIsbUJBQW1CO01BQ25CLDZCQUE2QjtJQUMvQjtJQUNBO01BQ0U7OztPQUdDO01BQ0QseUJBQXlCO01BQ3pCLFdBQVc7TUFDWCx5QkFBeUI7TUFDekIsbUJBQW1COztNQUVuQixnQkFBZ0I7SUFDbEI7SUFDQTtNQUNFLGdCQUFnQjtJQUNsQjtJQUNBO01BQ0UsY0FBYztJQUNoQjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jdXN0b21lci10YWJsZS9jdXN0b21lci10YWJsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogU3RydWN0dXJlICovXG4vKiB0YWJsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG59ICovXG4uZXhhbXBsZS1oZWFkZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG4ucmlnaHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMHB4O1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5vdXRlciB7XG4gICAgcGFkZGluZzogMC41ZW07XG59XG5cbmJvZHkge1xuICAgIGZvbnQtZmFtaWx5OiAnQ292ZXJlZCBCeSBZb3VyIEdyYWNlJywgY3Vyc2l2ZTtcbiAgICBsaW5lLWhlaWdodDogMS4yNTtcbn1cblxuLmZpbGwge1xuICAgIHdpZHRoOiAwLjVlbTtcbn1cblxuLmFjdGlvbnMge1xuICAgIGRpc3BsYXk6ZmxleCAhaW1wb3J0YW50O1xuICAgIGp1c3RpZnktY29udGVudDpmbGV4LWVuZCFpbXBvcnRhbnQ7XG59XG4gIFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTYwcHgpIHtcbiAgICAubWF0LXRhYmxlIHtcbiAgICAgIGJvcmRlcjogMDtcbiAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgfVxuICBcbiAgICAubWF0LXRhYmxlIGNhcHRpb24ge1xuICAgICAgZm9udC1zaXplOiAxZW07XG4gICAgfVxuICAgIFxuICAgIC8qICBFbmFibGUgdGhpcyB0byBoaWRlIGhlYWRlciovXG4gICAgLm1hdC10YWJsZSAubWF0LWhlYWRlci1jZWxsIHtcbiAgICAgIFxuICAgICAgYm9yZGVyOiAxMHB4IHNvbGlkO1xuICAgICAgY2xpcDogcmVjdCgwIDAgMCAwKTtcbiAgICAgIGhlaWdodDogMXB4O1xuICAgICAgbWFyZ2luOiAtMXB4O1xuICAgICAgcGFkZGluZzogMDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHdpZHRoOiAxcHg7XG4gICAgfVxuICAgIFxuICAgIC5tYXQtdGFibGUgLm1hdC1yb3cge1xuICAgICAgYm9yZGVyLWJvdHRvbTogNXB4IHNvbGlkICNkZGQ7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIC8qIGhlaWdodDogMjgwcHg7ICovXG4gICAgICAvKiBib3JkZXI6IDJweCBzb2xpZCByZWQ7ICovXG4gICAgfVxuICAgIC8qXG4gICAgLm1hdC10YWJsZSAubWF0LXJvdzpudGgtY2hpbGQoZXZlbikge2JhY2tncm91bmQ6ICNDQ0N9XG4gICAgLm1hdC10YWJsZSAubWF0LXJvdzpudGgtY2hpbGQob2RkKSB7YmFja2dyb3VuZDogI0ZGRn1cbiAgICAqL1xuICAgIC5tYXQtdGFibGUgLm1hdC1jZWxsIHtcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkO1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmb250LXNpemU6IDFlbTtcbiAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICBoZWlnaHQ6MzBweDtcbiAgICAgIG1hcmdpbi1ib3R0b206IDQlO1xuICAgICAgcGFkZGluZy1yaWdodDogMi4wZW07XG4gICAgICBwYWRkaW5nLWxlZnQ6IDIuMGVtO1xuICAgICAgLyogYm9yZGVyOiAycHggc29saWQgZ3JlZW47ICovXG4gICAgfVxuICAgIC5tYXQtdGFibGUgLm1hdC1jZWxsOmJlZm9yZSB7XG4gICAgICAvKlxuICAgICAgKiBhcmlhLWxhYmVsIGhhcyBubyBhZHZhbnRhZ2UsIGl0IHdvbid0IGJlIHJlYWQgaW5zaWRlIGEgdGFibGVcbiAgICAgIGNvbnRlbnQ6IGF0dHIoYXJpYS1sYWJlbCk7XG4gICAgICAqL1xuICAgICAgY29udGVudDogYXR0cihkYXRhLWxhYmVsKTtcbiAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgICBcbiAgICAgIGZvbnQtc2l6ZTogLjg1ZW07XG4gICAgfVxuICAgIC5tYXQtdGFibGUgLm1hdC1jZWxsOmxhc3QtY2hpbGQge1xuICAgICAgYm9yZGVyLWJvdHRvbTogMDtcbiAgICB9XG4gICAgLm1hdC10YWJsZSAubWF0LWNlbGw6Zmlyc3QtY2hpbGQge1xuICAgICAgbWFyZ2luLXRvcDogNCU7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/customer-table/customer-table.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/customer-table/customer-table.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"outer\">\n  <div class=\"example-header\">\n    <mat-form-field [style.width]='filterFieldWidth'>\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Busca\">\n    </mat-form-field>\n    <span [hidden]=\"simplified\" class=\"right\"><button mat-raised-button color=\"primary\" (click)=\"create()\">Novo Cliente</button></span>\n  </div>\n\n  <div class=\"example-container mat-elevation-z8\">\n\n    <mat-table [dataSource]=\"dataSource\" matSort>\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"name\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Nome </mat-header-cell>\n        <mat-cell *matCellDef=\"let row\" data-label=\"Nome\"> {{row.name}} </mat-cell>\n      </ng-container>\n\n      <!-- Reputation Column -->\n      <ng-container matColumnDef=\"reputation\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Reputação </mat-header-cell>\n        <mat-cell *matCellDef=\"let row\" data-label=\"Reputação\"> {{row.reputation}} </mat-cell>\n      </ng-container>\n\n      <!-- Mobile Column -->\n      <ng-container matColumnDef=\"mobile\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Celular </mat-header-cell>\n        <mat-cell *matCellDef=\"let row\" data-label=\"Celular\"> {{row.telephone[0] | customer: 'tel'}} </mat-cell>\n      </ng-container>\n\n      <!-- Details Column -->\n      <ng-container matColumnDef=\"actions\">\n        <mat-header-cell mat-header-cell *matHeaderCellDef></mat-header-cell>\n        <mat-cell class=\"actions\" *matCellDef=\"let row\" data-label=\"\">\n          <button mat-raised-button color=\"primary\" (click)=\"details(row)\">Detalhes</button>\n          <span class=\"fill\"></span>\n          <button mat-raised-button color=\"warn\" (click)=\"delete(row)\">Deletar</button> \n        </mat-cell>\n      </ng-container> \n\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\n      </mat-row>\n    </mat-table>\n\n    <mat-paginator [pageSizeOptions]=\"sizeOpt\" showFirstLastButtons></mat-paginator>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/customer-table/customer-table.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/customer-table/customer-table.component.ts ***!
  \***********************************************************************/
/*! exports provided: CustomerTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerTableComponent", function() { return CustomerTableComponent; });
/* harmony import */ var _confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../confirmation-dialog/confirmation-dialog.component */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _customer_form_customer_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../customer-form/customer-form.component */ "./src/app/components/customer-form/customer-form.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * @title Table with filtering
 */
var CustomerTableComponent = /** @class */ (function () {
    function CustomerTableComponent(customerService, dialog) {
        this.customerService = customerService;
        this.dialog = dialog;
        this.displayedColumns = ['name', 'reputation', 'mobile', 'actions'];
        this.sizeOpt = [5, 10, 20];
        this.simplified = false;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"]();
        this.filterFieldWidth = '50%';
        this.shared = _services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"].getInstance();
        this.customer = this.shared.newCustomer();
    }
    CustomerTableComponent.prototype.ngOnInit = function () {
        if (this.simplified) {
            this.displayedColumns = ['name', 'reputation', 'mobile'];
            this.sizeOpt = [1, 3, 5];
            this.filterFieldWidth = '100%';
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate = function (data, filter) {
            return data.name.toLowerCase().includes(filter) ||
                data.reputation.toLowerCase().includes(filter) ||
                data.telephone[0].toString().includes(filter);
        };
        this.findAll();
    };
    CustomerTableComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    CustomerTableComponent.prototype.findAll = function () {
        var _this = this;
        this.customerService.findAll().subscribe(function (responseApi) {
            _this.dataSource.data = responseApi.data;
        }, function (err) {
            console.log(err['error']['errors'][0]);
        });
    };
    CustomerTableComponent.prototype.findById = function (id) {
        var _this = this;
        this.customerService.findById(id).subscribe(function (responseApi) {
            _this.customer = responseApi.data;
        }, function (err) {
            console.log(err['error']['errors'][0]);
        });
    };
    CustomerTableComponent.prototype.create = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_customer_form_customer_form_component__WEBPACK_IMPORTED_MODULE_5__["CustomerFormComponent"], {
            data: {
                customer: this.shared.newCustomer(),
                close: 'Cancelar',
                ok: 'Criar'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            _this.findAll();
        });
    };
    CustomerTableComponent.prototype.details = function (customer) {
        var _this = this;
        var dialogRef = this.dialog.open(_customer_form_customer_form_component__WEBPACK_IMPORTED_MODULE_5__["CustomerFormComponent"], {
            data: {
                customer: customer,
                close: 'Fechar',
                ok: 'Salvar'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            _this.findAll();
        });
    };
    CustomerTableComponent.prototype.delete = function (customer) {
        var _this = this;
        // console.log(customer);
        var dialogRef = this.dialog.open(_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_0__["ConfirmationDialogComponent"]);
        dialogRef.componentInstance.message = "Deletar " + customer.name + " permanentemente?";
        dialogRef.afterClosed().subscribe(function (result) {
            console.log(result);
            if (result) {
                _this.customerService.delete(customer.id).subscribe(function (responseApi) {
                    _this.findAll();
                });
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], CustomerTableComponent.prototype, "simplified", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], CustomerTableComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], CustomerTableComponent.prototype, "sort", void 0);
    CustomerTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-customer-table',
            template: __webpack_require__(/*! ./customer-table.component.html */ "./src/app/components/customer-table/customer-table.component.html"),
            styles: [__webpack_require__(/*! ./customer-table.component.css */ "./src/app/components/customer-table/customer-table.component.css")]
        }),
        __metadata("design:paramtypes", [_services_customer_customer_service__WEBPACK_IMPORTED_MODULE_1__["CustomerService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]])
    ], CustomerTableComponent);
    return CustomerTableComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".grid-container {\n  margin: 20px;\n}\n\n.dashboard-card {\n  position: absolute;\n  top: 15px;\n  left: 15px;\n  right: 15px;\n  bottom: 15px;\n}\n\n.more-button {\n  position: absolute;\n  top: 5px;\n  right: 10px;\n}\n\n.dashboard-card-content {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFVBQVU7RUFDVixXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ncmlkLWNvbnRhaW5lciB7XG4gIG1hcmdpbjogMjBweDtcbn1cblxuLmRhc2hib2FyZC1jYXJkIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDE1cHg7XG4gIGxlZnQ6IDE1cHg7XG4gIHJpZ2h0OiAxNXB4O1xuICBib3R0b206IDE1cHg7XG59XG5cbi5tb3JlLWJ1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1cHg7XG4gIHJpZ2h0OiAxMHB4O1xufVxuXG4uZGFzaGJvYXJkLWNhcmQtY29udGVudCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-grid-list cols=1 rowHeight=\"560px\">\n  <mat-grid-tile>\n    <mat-card class=\"dashboard-card\">\n      <mat-card-header>\n        <mat-card-title>\n          <h2>Relatório Geral</h2>\n          <!-- <button mat-icon-button class=\"more-button\" [matMenuTriggerFor]=\"menu\" aria-label=\"Toggle menu\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\" xPosition=\"before\">\n            <button mat-menu-item>Expand</button>\n            <button mat-menu-item>Remove</button>\n          </mat-menu> -->\n        </mat-card-title>\n      </mat-card-header>\n      <mat-card-content class=\"dashboard-card-content\">\n        <app-graphic></app-graphic>\n      </mat-card-content>\n      <mat-card-footer>\n        <!-- <p>Entrada: R$ 100.000,00  Saída: R$ 50.000,00</p> -->\n      </mat-card-footer>\n    </mat-card>\n  </mat-grid-tile>\n  <mat-grid-tile>\n    <mat-card class=\"dashboard-card\" [style.overflow]=\"'auto'\" [style.height.px]=\"'482'\">\n      <mat-card-header>\n        <mat-card-title>\n          <h2>Clientes</h2>\n          <!-- <button mat-icon-button class=\"more-button\" [matMenuTriggerFor]=\"menu\" aria-label=\"Toggle menu\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\" xPosition=\"before\">\n            <button mat-menu-item>Expand</button>\n            <button mat-menu-item>Remove</button>\n          </mat-menu> -->\n        </mat-card-title>\n      </mat-card-header>\n      <mat-card-content class=\"dashboard-card-content\">\n        <app-customer-table [simplified]=\"true\"></app-customer-table>\n      </mat-card-content>\n      <mat-card-footer>\n\n      </mat-card-footer>\n    </mat-card>\n  </mat-grid-tile>\n</mat-grid-list>\n"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/components/dashboard/dashboard.component.css")]
        })
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/components/graphic/graphic.component.css":
/*!**********************************************************!*\
  !*** ./src/app/components/graphic/graphic.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".chart {\n    width: 100%;\n    height: 200px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ncmFwaGljL2dyYXBoaWMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7SUFDWCxhQUFhO0FBQ2pCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9ncmFwaGljL2dyYXBoaWMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jaGFydCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAyMDBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/graphic/graphic.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/graphic/graphic.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"text-center\">\n  <strong>Período: 1 Jan, 2018 - 30 Jul, 2018</strong>\n</p>\n<div class=\"chart\">\n  <canvas id=\"areaChart\"></canvas>\n</div>\n"

/***/ }),

/***/ "./src/app/components/graphic/graphic.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/graphic/graphic.component.ts ***!
  \*********************************************************/
/*! exports provided: GraphicComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GraphicComponent", function() { return GraphicComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/src/chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GraphicComponent = /** @class */ (function () {
    function GraphicComponent() {
    }
    GraphicComponent.prototype.ngOnInit = function () {
        this.chart = new chart_js__WEBPACK_IMPORTED_MODULE_1__["Chart"]('areaChart', {
            type: 'line',
            data: {
                labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho'],
                datasets: [
                    {
                        label: 'Entrada',
                        backgroundColor: 'rgba(63,226,38,0.9)',
                        borderColor: 'rgba(63,226,38,1)',
                        pointBackgroundColor: 'rgba(63,226,38,1)',
                        pointBorderColor: 'rgba(63,226,38,1)',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(63,226,38,1)',
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: 'Saída',
                        backgroundColor: 'rgba(236,23,23,0.9)',
                        borderColor: 'rgba(236,23,23,1)',
                        pointBackgroundColor: 'rgba(236,23,23,1)',
                        pointBorderColor: 'rgba(236,23,23,1)',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(236,23,23,1)',
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                            display: true
                        }],
                    yAxes: [{
                            display: true
                        }]
                },
                elements: {
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 10
                    }
                }
            }
        });
    };
    GraphicComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-graphic',
            template: __webpack_require__(/*! ./graphic.component.html */ "./src/app/components/graphic/graphic.component.html"),
            styles: [__webpack_require__(/*! ./graphic.component.css */ "./src/app/components/graphic/graphic.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GraphicComponent);
    return GraphicComponent;
}());



/***/ }),

/***/ "./src/app/components/navigation/navigation.component.css":
/*!****************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  text-align: left;\n}\n\n.example-is-mobile .example-toolbar {\n  position: fixed;\n  /* Make sure the toolbar will stay on top of the content as it scrolls past. */\n  z-index: 2;\n}\n\nh1.example-app-name {\n  margin-top: 8px;\n  /* margin-left: 8px; */\n  font-size: 1.5em;\n  /* text-align: center */\n}\n\n.example-toolbar span {\n  display: inline;\n  vertical-align: middle;\n  /* margin: 0 auto; */\n}\n\n.example-toolbar img {\n  float: left;\n  margin-right: 8px;\n}\n\n.example-sidenav-container {\n  /* When the sidenav is not fixed, stretch the sidenav container to fill the available space. This\n     causes `<mat-sidenav-content>` to act as our scrolling element for desktop layouts. */\n  flex: 1;\n}\n\n.example-is-mobile .example-sidenav-container {\n  /* When the sidenav is fixed, don't constrain the height of the sidenav container. This allows the\n     `<body>` to be our scrolling element for mobile layouts. */\n  flex: 1 0 auto;\n}\n\n.fill-space {\n  /* This fills the remaining space, by using flexbox.\n   Every toolbar row uses a flexbox row layout.*/\n  flex: 1 1 auto;\n}\n\n.disabled {\n  pointer-events:none;\n  opacity:0.6;\n}\n\na.nostyle:link {\n  text-decoration: inherit;\n  color: inherit;\n  /*cursor: auto;*/\n}\n\na.nostyle:visited {\n  text-decoration: inherit;\n  color: inherit;\n  /*cursor: auto;*/\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9uYXZpZ2F0aW9uL25hdmlnYXRpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixTQUFTO0VBQ1QsT0FBTztFQUNQLFFBQVE7RUFDUixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsOEVBQThFO0VBQzlFLFVBQVU7QUFDWjs7QUFFQTtFQUNFLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFOzBGQUN3RjtFQUN4RixPQUFPO0FBQ1Q7O0FBRUE7RUFDRTsrREFDNkQ7RUFDN0QsY0FBYztBQUNoQjs7QUFFQTtFQUNFO2dEQUM4QztFQUM5QyxjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7QUFDYjs7QUFFQTtFQUNFLHdCQUF3QjtFQUN4QixjQUFjO0VBQ2QsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0Usd0JBQXdCO0VBQ3hCLGNBQWM7RUFDZCxnQkFBZ0I7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5leGFtcGxlLWlzLW1vYmlsZSAuZXhhbXBsZS10b29sYmFyIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBNYWtlIHN1cmUgdGhlIHRvb2xiYXIgd2lsbCBzdGF5IG9uIHRvcCBvZiB0aGUgY29udGVudCBhcyBpdCBzY3JvbGxzIHBhc3QuICovXG4gIHotaW5kZXg6IDI7XG59XG5cbmgxLmV4YW1wbGUtYXBwLW5hbWUge1xuICBtYXJnaW4tdG9wOiA4cHg7XG4gIC8qIG1hcmdpbi1sZWZ0OiA4cHg7ICovXG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIC8qIHRleHQtYWxpZ246IGNlbnRlciAqL1xufVxuXG4uZXhhbXBsZS10b29sYmFyIHNwYW4ge1xuICBkaXNwbGF5OiBpbmxpbmU7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIC8qIG1hcmdpbjogMCBhdXRvOyAqL1xufVxuXG4uZXhhbXBsZS10b29sYmFyIGltZyB7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tcmlnaHQ6IDhweDtcbn1cblxuLmV4YW1wbGUtc2lkZW5hdi1jb250YWluZXIge1xuICAvKiBXaGVuIHRoZSBzaWRlbmF2IGlzIG5vdCBmaXhlZCwgc3RyZXRjaCB0aGUgc2lkZW5hdiBjb250YWluZXIgdG8gZmlsbCB0aGUgYXZhaWxhYmxlIHNwYWNlLiBUaGlzXG4gICAgIGNhdXNlcyBgPG1hdC1zaWRlbmF2LWNvbnRlbnQ+YCB0byBhY3QgYXMgb3VyIHNjcm9sbGluZyBlbGVtZW50IGZvciBkZXNrdG9wIGxheW91dHMuICovXG4gIGZsZXg6IDE7XG59XG5cbi5leGFtcGxlLWlzLW1vYmlsZSAuZXhhbXBsZS1zaWRlbmF2LWNvbnRhaW5lciB7XG4gIC8qIFdoZW4gdGhlIHNpZGVuYXYgaXMgZml4ZWQsIGRvbid0IGNvbnN0cmFpbiB0aGUgaGVpZ2h0IG9mIHRoZSBzaWRlbmF2IGNvbnRhaW5lci4gVGhpcyBhbGxvd3MgdGhlXG4gICAgIGA8Ym9keT5gIHRvIGJlIG91ciBzY3JvbGxpbmcgZWxlbWVudCBmb3IgbW9iaWxlIGxheW91dHMuICovXG4gIGZsZXg6IDEgMCBhdXRvO1xufVxuXG4uZmlsbC1zcGFjZSB7XG4gIC8qIFRoaXMgZmlsbHMgdGhlIHJlbWFpbmluZyBzcGFjZSwgYnkgdXNpbmcgZmxleGJveC5cbiAgIEV2ZXJ5IHRvb2xiYXIgcm93IHVzZXMgYSBmbGV4Ym94IHJvdyBsYXlvdXQuKi9cbiAgZmxleDogMSAxIGF1dG87XG59XG5cbi5kaXNhYmxlZCB7XG4gIHBvaW50ZXItZXZlbnRzOm5vbmU7XG4gIG9wYWNpdHk6MC42O1xufVxuXG5hLm5vc3R5bGU6bGluayB7XG4gIHRleHQtZGVjb3JhdGlvbjogaW5oZXJpdDtcbiAgY29sb3I6IGluaGVyaXQ7XG4gIC8qY3Vyc29yOiBhdXRvOyovXG59XG5cbmEubm9zdHlsZTp2aXNpdGVkIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBpbmhlcml0O1xuICBjb2xvcjogaW5oZXJpdDtcbiAgLypjdXJzb3I6IGF1dG87Ki9cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/navigation/navigation.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\" [class.example-is-mobile]=\"mobileQuery.matches\">\n  <div [hidden]=\"!showTemplate\">\n    <mat-toolbar color=\"primary\" class=\"example-toolbar\">\n      <a class=\"nostyle\" routerLink=\".\"><img src=\"/assets/img/hair-cut.svg\" height=\"40em\" width=\"40em\"><h1 class=\"example-app-name\">Tati Hair</h1></a>\n      <span class=\"fill-space\"></span>\n      <button mat-icon-button (click)=\"openUserInfo()\"><mat-icon>account_circle</mat-icon></button>\n      <button mat-icon-button (click)=\"snav.toggle()\"><mat-icon>menu</mat-icon></button>\n    </mat-toolbar>\n  </div>\n\n  <mat-sidenav-container class=\"example-sidenav-container\"\n                         [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\">\n    <mat-sidenav #snav [mode]=\"mobileQuery.matches ? 'over' : 'side'\"\n                 [fixedInViewport]=\"mobileQuery.matches\" fixedTopGap=\"56\" position=\"end\">\n      <mat-nav-list>\n        <a mat-list-item routerLink=\".\"><i class=\"material-icons\">dashboard</i>Dashboard</a>\n        <a mat-list-item routerLink=\"clientes\"><i class=\"material-icons\">contacts</i>Clientes</a>\n        <a mat-list-item routerLink=\"agenda\"><i class=\"material-icons\">event</i>Agenda</a>\n        <a mat-list-item routerLink=\".\" class=\"disabled\"><i class=\"material-icons\">ballot</i>Estoque</a>\n        <a mat-list-item routerLink=\".\" class=\"disabled\"><i class=\"material-icons\">airline_seat_recline_extra</i>Serviços</a>\n        <a mat-list-item routerLink=\".\" class=\"disabled\"><i class=\"material-icons\">poll</i>Caixa</a>\n      </mat-nav-list>\n    </mat-sidenav>\n\n    <mat-sidenav-content>\n      <router-outlet></router-outlet>\n    </mat-sidenav-content>\n  </mat-sidenav-container>\n</div>\n"

/***/ }),

/***/ "./src/app/components/navigation/navigation.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.ts ***!
  \***************************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_info_user_info_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user-info/user-info.component */ "./src/app/components/user-info/user-info.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/** @title Responsive sidenav */
var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(changeDetectorRef, media, dialog) {
        this.dialog = dialog;
        this.showTemplate = false;
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = function () { return changeDetectorRef.detectChanges(); };
        this.mobileQuery.addListener(this._mobileQueryListener);
        this.shared = _services_shared_service__WEBPACK_IMPORTED_MODULE_0__["SharedService"].getInstance();
    }
    NavigationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.showTemplate.subscribe(function (show) {
            console.log('showTemplate', show);
            return _this.showTemplate = show;
        });
        this.showTemplate = this.shared.user !== null;
        console.log('showTemplate', this.showTemplate);
    };
    NavigationComponent.prototype.ngOnDestroy = function () {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    };
    NavigationComponent.prototype.openUserInfo = function () {
        var dialogRef = this.dialog.open(_user_info_user_info_component__WEBPACK_IMPORTED_MODULE_3__["UserInfoComponent"]);
    };
    NavigationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-navigation',
            template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/components/navigation/navigation.component.html"),
            styles: [__webpack_require__(/*! ./navigation.component.css */ "./src/app/components/navigation/navigation.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["MediaMatcher"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/components/security/auth.guard.ts":
/*!***************************************************!*\
  !*** ./src/app/components/security/auth.guard.ts ***!
  \***************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuard = /** @class */ (function () {
    function AuthGuard(userService, router) {
        this.userService = userService;
        this.router = router;
        this.shared = _services_shared_service__WEBPACK_IMPORTED_MODULE_3__["SharedService"].getInstance();
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        console.log(sessionStorage.getItem('user'));
        if (this.shared.isLoggedIn()) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_services_user_user_service__WEBPACK_IMPORTED_MODULE_0__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/components/security/auth.interceptor.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/security/auth.interceptor.ts ***!
  \*********************************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/shared.service */ "./src/app/services/shared.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor() {
        this.shared = _services_shared_service__WEBPACK_IMPORTED_MODULE_1__["SharedService"].getInstance();
    }
    AuthInterceptor.prototype.intercept = function (req, next) {
        var authRequest;
        if (this.shared.isLoggedIn()) {
            authRequest = req.clone({
                setHeaders: {
                    'Authorization': this.shared.token
                }
            });
            return next.handle(authRequest);
        }
        else {
            return next.handle(req);
        }
    };
    AuthInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AuthInterceptor);
    return AuthInterceptor;
}());



/***/ }),

/***/ "./src/app/components/security/login/login.component.css":
/*!***************************************************************!*\
  !*** ./src/app/components/security/login/login.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-logo {\n    color: whitesmoke;\n}\n\n.example-icon {\n    padding: 0 14px;\n}\n\n.example-spacer {\n    flex: 1 1 auto;\n}\n\n.example-card {\n    width: 15em;\n    margin: 10% auto;\n}\n\n.mat-card-title{\n    font-size: 1.8em;\n}\n\n.mat-form-field {\n    width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zZWN1cml0eS9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBQ0E7SUFDSSxXQUFXO0lBQ1gsZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksV0FBVztBQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zZWN1cml0eS9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luLWxvZ28ge1xuICAgIGNvbG9yOiB3aGl0ZXNtb2tlO1xufVxuXG4uZXhhbXBsZS1pY29uIHtcbiAgICBwYWRkaW5nOiAwIDE0cHg7XG59XG5cbi5leGFtcGxlLXNwYWNlciB7XG4gICAgZmxleDogMSAxIGF1dG87XG59XG4uZXhhbXBsZS1jYXJkIHtcbiAgICB3aWR0aDogMTVlbTtcbiAgICBtYXJnaW46IDEwJSBhdXRvO1xufVxuLm1hdC1jYXJkLXRpdGxle1xuICAgIGZvbnQtc2l6ZTogMS44ZW07XG59XG4ubWF0LWZvcm0tZmllbGQge1xuICAgIHdpZHRoOiAxMDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/security/login/login.component.html":
/*!****************************************************************!*\
  !*** ./src/app/components/security/login/login.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #form=\"ngForm\" (ngSubmit)=\"login()\" novalidate>\n  <mat-card class=\"example-card\">\n    <mat-card-header>\n      <mat-card-title><b>Tati</b>Hair</mat-card-title>\n    </mat-card-header>\n    <br/>\n    <p>Entre com usuário e senha.</p>\n\n    <mat-card-content>\n        <mat-form-field>\n          <mat-icon matPrefix>person</mat-icon>\n          <input matInput type=\"email\"\n                      [(ngModel)]=\"user.email\"\n                      name=\"email\"\n                      class=\"form-control\"\n                      id=\"inputEmail\"\n                      placeholder=\"Email\"\n                      #email=\"ngModel\"\n                      email required>\n        </mat-form-field>\n        <br/>\n        <mat-form-field>\n          <mat-icon matPrefix>enhanced_encryption</mat-icon>\n          <input matInput placeholder=\"Senha\"\n                [(ngModel)]=\"user.password\"\n                [type]=\"hide ? 'password' : 'text'\"\n                name=\"password\"\n                id=\"inputPassword\"\n                #name=\"ngModel\"\n                required>\n          <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}}</mat-icon>\n        </mat-form-field>\n\n    </mat-card-content>\n    <mat-card-actions fxLayout=\"row\" fxLayoutAlign=\"flex-end\">\n      <button mat-raised-button type=\"button\" (click)=\"cancelLogin()\">Cancelar</button>\n      <button mat-raised-button type=\"submit\" [disabled]=\"!form.valid\" color=\"primary\">Entrar</button>\n    </mat-card-actions>\n  </mat-card>\n</form>\n"

/***/ }),

/***/ "./src/app/components/security/login/login.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/security/login/login.component.ts ***!
  \**************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../model/user */ "./src/app/model/user.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoginComponent = /** @class */ (function () {
    function LoginComponent(userService, router, iconRegistry, sanitizer, snackBar) {
        this.userService = userService;
        this.router = router;
        this.snackBar = snackBar;
        this.user = new _model_user__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        this.hide = true;
        this.shared = _services_shared_service__WEBPACK_IMPORTED_MODULE_0__["SharedService"].getInstance();
        iconRegistry.addSvgIcon('tati', sanitizer.bypassSecurityTrustResourceUrl('/src/assets/img/hair-cut.svg'));
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        // console.log("ip");
        // this.userService.getIpAddress().subscribe(data => {
        //   console.log(data);
        // });
        this.userService.login(this.user).subscribe(function (userAuthentication) {
            _this.shared.logIn(userAuthentication);
            _this.router.navigate(['/']);
            console.log('Login');
        }, function (err) {
            _this.shared.logOut();
            console.log('Erro');
            _this.snackBar.open('Erro de conexão!', 'Fechar');
        });
    };
    LoginComponent.prototype.cancelLogin = function () {
        this.user = new _model_user__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        this.form.resetForm();
        // window.location.href = '/login';
        // window.location.reload();
    };
    LoginComponent.prototype.getFormGroupClass = function (isInvalid, isDirty) {
        return {
            'form-group': true,
            'has-error': isInvalid && isDirty,
            'has-success': !isInvalid && isDirty
        };
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"])('form'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgForm"])
    ], LoginComponent.prototype, "form", void 0);
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/security/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/security/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_services_user_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconRegistry"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["DomSanitizer"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/user-info/user-info.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/user-info/user-info.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h3 {\n  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif\n}\n\n.buttons {\n  display: flex;\n  align-items: center;\n  align-content: space-between;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2VyLWluZm8vdXNlci1pbmZvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRTtBQUNGOztBQUVBO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw0QkFBNEI7QUFDOUIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3VzZXItaW5mby91c2VyLWluZm8uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImgzIHtcbiAgZm9udC1mYW1pbHk6IC1hcHBsZS1zeXN0ZW0sIEJsaW5rTWFjU3lzdGVtRm9udCwgJ1NlZ29lIFVJJywgUm9ib3RvLCBPeHlnZW4sIFVidW50dSwgQ2FudGFyZWxsLCAnT3BlbiBTYW5zJywgJ0hlbHZldGljYSBOZXVlJywgc2Fucy1zZXJpZlxufVxuXG4uYnV0dG9ucyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGFsaWduLWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/user-info/user-info.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/user-info/user-info.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-dialog-content>\n  <h3 *ngIf=\"shared.user\">{{ shared.user.email }}</h3>\n</mat-dialog-content>\n<mat-dialog-actions fxLayout=\"row\" fxLayoutAlign=\"flex-end\">\n  <button mat-raised-button color=\"secondary\" (click)=\"close()\">Fechar</button>\n  <button mat-raised-button color=\"primary\" (click)=\"logOut()\">Sair</button>\n</mat-dialog-actions>\n"

/***/ }),

/***/ "./src/app/components/user-info/user-info.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/user-info/user-info.component.ts ***!
  \*************************************************************/
/*! exports provided: UserInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserInfoComponent", function() { return UserInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_shared_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserInfoComponent = /** @class */ (function () {
    function UserInfoComponent(router, dialogRef) {
        this.router = router;
        this.dialogRef = dialogRef;
        this.shared = src_app_services_shared_service__WEBPACK_IMPORTED_MODULE_1__["SharedService"].getInstance();
    }
    UserInfoComponent.prototype.ngOnInit = function () {
    };
    UserInfoComponent.prototype.logOut = function () {
        this.dialogRef.close();
        this.shared.logOut();
        this.router.navigate(['/login']);
    };
    UserInfoComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    UserInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-info',
            template: __webpack_require__(/*! ./user-info.component.html */ "./src/app/components/user-info/user-info.component.html"),
            styles: [__webpack_require__(/*! ./user-info.component.css */ "./src/app/components/user-info/user-info.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]])
    ], UserInfoComponent);
    return UserInfoComponent;
}());



/***/ }),

/***/ "./src/app/material.ts":
/*!*****************************!*\
  !*** ./src/app/material.ts ***!
  \*****************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material-moment-adapter */ "./node_modules/@angular/material-moment-adapter/esm5/material-moment-adapter.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var modules = [
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatAutocompleteModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonToggleModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCardModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCheckboxModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatChipsModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDatepickerModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatExpansionModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatGridListModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatListModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatMenuModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginatorModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBarModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressSpinnerModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatRadioModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatRippleModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelectModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSidenavModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSliderModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSlideToggleModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBarModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSortModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTabsModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTooltipModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatStepperModule"],
    _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_2__["MatMomentDateModule"],
];
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: modules.slice(),
            exports: modules.slice(),
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/model/customer.ts":
/*!***********************************!*\
  !*** ./src/app/model/customer.ts ***!
  \***********************************/
/*! exports provided: Customer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Customer", function() { return Customer; });
var Customer = /** @class */ (function () {
    function Customer(id, name, reputation, gender, telephone, email, birthday, cpf, period, notes, dateEntered) {
        this.id = id;
        this.name = name;
        this.reputation = reputation;
        this.gender = gender;
        this.telephone = telephone;
        this.email = email;
        this.birthday = birthday;
        this.cpf = cpf;
        this.period = period;
        this.notes = notes;
        this.dateEntered = dateEntered;
    }
    return Customer;
}());



/***/ }),

/***/ "./src/app/model/user.ts":
/*!*******************************!*\
  !*** ./src/app/model/user.ts ***!
  \*******************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(id, email, password, profile) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.profile = profile;
    }
    return User;
}());



/***/ }),

/***/ "./src/app/pipes/customer.pipe.ts":
/*!****************************************!*\
  !*** ./src/app/pipes/customer.pipe.ts ***!
  \****************************************/
/*! exports provided: CustomerPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerPipe", function() { return CustomerPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CustomerPipe = /** @class */ (function () {
    function CustomerPipe() {
    }
    CustomerPipe.prototype.transform = function (value, args) {
        if (args === 'cpf') {
            return value.substring(0, 3) + '.' + value.substring(3, 6) +
                '.' + value.substring(6, 9) + '-' + value.substring(9, 11);
        }
        else if (args === 'tel') {
            return '(' + value.substring(0, 2) + ') ' +
                value.substring(2, 7) + '-' + value.substring(7, 11);
        }
        else if (args === 'rep') {
            if (value === 'Good') {
                return 'Boa';
            }
            else if (value === 'Attention') {
                return 'Atenção';
            }
            else if (value === 'Bad') {
                return 'Ruim';
            }
        }
        // this.cpf = this.cpf.substring(0, 3) + '.' + this.cpf.substring(3, 6) +
        //   '.' + this.cpf.substring(6, 9) + '-' + this.cpf.substring(9, 11);
        // this.telephone = '(' + this.telephone.substring(0, 2) + ')' +
        //   this.telephone.substring(2, 7) + '-' + this.telephone.substring(7, 11);
        return value;
    };
    CustomerPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'customer'
        })
    ], CustomerPipe);
    return CustomerPipe;
}());



/***/ }),

/***/ "./src/app/services/customer/customer.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/customer/customer.service.ts ***!
  \*******************************************************/
/*! exports provided: CustomerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerService", function() { return CustomerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _mercurius_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../mercurius.api */ "./src/app/services/mercurius.api.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CustomerService = /** @class */ (function () {
    function CustomerService(http) {
        this.http = http;
    }
    CustomerService.prototype.createOrUpdate = function (customer) {
        if (customer.id !== null && customer.id !== '') {
            return this.http.put(_mercurius_api__WEBPACK_IMPORTED_MODULE_2__["HELP_DESK_API"] + "/api/customer", customer);
        }
        else {
            customer.id = null;
            return this.http.post(_mercurius_api__WEBPACK_IMPORTED_MODULE_2__["HELP_DESK_API"] + "/api/customer", customer);
        }
    };
    CustomerService.prototype.findAll = function () {
        return this.http.get(_mercurius_api__WEBPACK_IMPORTED_MODULE_2__["HELP_DESK_API"] + "/api/customer");
    };
    CustomerService.prototype.findById = function (id) {
        return this.http.get(_mercurius_api__WEBPACK_IMPORTED_MODULE_2__["HELP_DESK_API"] + "/api/customer/" + id);
    };
    CustomerService.prototype.delete = function (id) {
        return this.http.delete(_mercurius_api__WEBPACK_IMPORTED_MODULE_2__["HELP_DESK_API"] + "/api/customer/" + id);
    };
    CustomerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CustomerService);
    return CustomerService;
}());



/***/ }),

/***/ "./src/app/services/mercurius.api.ts":
/*!*******************************************!*\
  !*** ./src/app/services/mercurius.api.ts ***!
  \*******************************************/
/*! exports provided: HELP_DESK_API */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HELP_DESK_API", function() { return HELP_DESK_API; });
var HELP_DESK_API = 'https://mercurius-app.herokuapp.com';


/***/ }),

/***/ "./src/app/services/shared.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/shared.service.ts ***!
  \********************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_customer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/customer */ "./src/app/model/customer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SharedService = /** @class */ (function () {
    function SharedService() {
        this.showTemplate = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        console.log('SharedService [constructor]');
        this.user = JSON.parse(sessionStorage.getItem('user'));
        this.token = sessionStorage.getItem('token');
        this.showTemplate.emit(true);
        return SharedService_1.instance = SharedService_1.instance || this;
    }
    SharedService_1 = SharedService;
    SharedService.getInstance = function () {
        if (this.instance == null) {
            this.instance = new SharedService_1();
        }
        return this.instance;
    };
    SharedService.prototype.newCustomer = function () {
        return new _model_customer__WEBPACK_IMPORTED_MODULE_1__["Customer"]('', '', '', '', new Array(), new Array(), null, '', 0, '', new Date());
    };
    SharedService.prototype.isLoggedIn = function () {
        // console.log(this.token);
        if (this.user == null) {
            return false;
        }
        return this.user.email !== '';
    };
    SharedService.prototype.logIn = function (userAuthentication) {
        sessionStorage.setItem('token', userAuthentication.token);
        sessionStorage.setItem('user', JSON.stringify(userAuthentication.user));
        this.token = userAuthentication.token;
        this.user = userAuthentication.user;
        this.user.profile = this.user.profile.substring(5);
        this.showTemplate.emit(true);
    };
    SharedService.prototype.logOut = function () {
        sessionStorage.removeItem('user');
        sessionStorage.removeItem('token');
        this.user = null;
        this.token = null;
        this.showTemplate.emit(false);
    };
    var SharedService_1;
    SharedService.instance = null;
    SharedService = SharedService_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], SharedService);
    return SharedService;
}());



/***/ }),

/***/ "./src/app/services/user/user.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/user/user.service.ts ***!
  \***********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _mercurius_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../mercurius.api */ "./src/app/services/mercurius.api.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.login = function (user) {
        return this.http.post(_mercurius_api__WEBPACK_IMPORTED_MODULE_4__["HELP_DESK_API"] + "/api/auth", user);
    };
    UserService.prototype.createOrUpdate = function (user) {
        if (user.id != null && user.id !== '') {
            return this.http.put(_mercurius_api__WEBPACK_IMPORTED_MODULE_4__["HELP_DESK_API"] + "/api/user", user);
        }
        else {
            user.id = null;
            return this.http.post(_mercurius_api__WEBPACK_IMPORTED_MODULE_4__["HELP_DESK_API"] + "/api/user", user);
        }
    };
    UserService.prototype.findAll = function (page, count) {
        return this.http.get(_mercurius_api__WEBPACK_IMPORTED_MODULE_4__["HELP_DESK_API"] + "/api/user/" + page + "/" + count);
    };
    UserService.prototype.findById = function (id) {
        return this.http.get(_mercurius_api__WEBPACK_IMPORTED_MODULE_4__["HELP_DESK_API"] + "/api/user/" + id);
    };
    UserService.prototype.delete = function (id) {
        return this.http.delete(_mercurius_api__WEBPACK_IMPORTED_MODULE_4__["HELP_DESK_API"] + "/api/user/" + id);
    };
    // Get IP Adress using http://freegeoip.net/json/?callback
    UserService.prototype.getIpAddress = function () {
        return this.http
            .get('http://freegeoip.net/json/?callback')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (response) { return response || {}; }));
    };
    UserService.prototype.handleError = function (error) {
        // Log error in the browser console
        console.error('observable error: ', error);
        return rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"].throw(error);
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    API_URL: '//dev.com'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/grosa/Dev/mercurius/angular-material/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map