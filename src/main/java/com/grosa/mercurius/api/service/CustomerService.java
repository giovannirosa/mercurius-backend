package com.grosa.mercurius.api.service;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.grosa.mercurius.api.entity.Customer;

@Component
public interface CustomerService {
	
	Customer createOrUpdate(Customer customer);
	
	Customer findById(String id);
	
	void delete(String id);
	
	Page<Customer> list(int page, int count);
	
	Iterable<Customer> findAll();
	
	Page<Customer> findByParameters(int page, int count,String name,String reputation,
			String telephone,Date startDt,Date endDt,String cpf,int startPeriod, int endPeriod);

}
