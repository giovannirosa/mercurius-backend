package com.grosa.mercurius.api.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.grosa.mercurius.api.entity.Customer;
import com.grosa.mercurius.api.repository.CustomerRepository;
import com.grosa.mercurius.api.service.CustomerService;

@Component
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerRepository customerRepo;

	@Override
	public Customer createOrUpdate(Customer customer) {
		return this.customerRepo.save(customer);
	}

	@Override
	public Customer findById(String id) {
		return this.customerRepo.findById(id).get();
	}

	@Override
	public void delete(String id) {
		this.customerRepo.deleteById(id);
	}

	@Override
	public Page<Customer> list(int page, int count) {
		Pageable pages = PageRequest.of(page, count);
		return this.customerRepo.findAll(pages);
	}

	@Override
	public Iterable<Customer> findAll() {
		return this.customerRepo.findAll(Sort.by("name"));
	}
	
	@Override
	public Page<Customer> findByParameters(int page, int count,String name,String reputation,
			String telephone,Date startDt,Date endDt,String cpf,int startPeriod, int endPeriod) {
		Pageable pages = PageRequest.of(page, count);
		return this.customerRepo.
				findByNameIgnoreCaseContainingAndReputationIgnoreCaseContainingAndTelephoneIgnoreCaseContainingAndBirthdayBetweenAndCpfIgnoreCaseContainingAndPeriodBetweenOrderByName(
						name, reputation, telephone, startDt, endDt, cpf, startPeriod, endPeriod, pages);
	}

}
