package com.grosa.mercurius.api.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grosa.mercurius.api.entity.Customer;
import com.grosa.mercurius.api.response.Response;
import com.grosa.mercurius.api.security.jwt.JwtTokenUtil;
import com.grosa.mercurius.api.service.CustomerService;

@RestController
@RequestMapping("/api/customer")
@CrossOrigin(origins = "*")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
    protected JwtTokenUtil jwtTokenUtil;
	
	@PostMapping()
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<Customer>> create(HttpServletRequest request, @RequestBody Customer customer,
			BindingResult result) {
		Response<Customer> response = new Response<>();
		try {
			customer.setDateEntered(new Date());
			validateCreateCustomer(customer, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}
			Customer customerPersisted = (Customer) customerService.createOrUpdate(customer);
			response.setData(customerPersisted);
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	private void validateCreateCustomer(Customer customer, BindingResult result) {
		if (customer.getName() == null) {
			result.addError(new ObjectError("Customer", "Nome não informado"));
			return;
		}
		if (customer.getReputation() == null) {
			result.addError(new ObjectError("Customer", "Reputação não informada"));
			return;
		}
		if (customer.getGender() == null) {
			result.addError(new ObjectError("Customer", "Sexo não informado"));
			return;
		}
		if (customer.getTelephone().get(0) == null) {
			result.addError(new ObjectError("Customer", "Celular não informado"));
			return;
		}
	}
	
	@PutMapping()
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<Customer>> update(HttpServletRequest request, @RequestBody Customer customer,
			BindingResult result) {
		Response<Customer> response = new Response<Customer>();
		try {
			validateUpdateCustomer(customer, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}
			Customer customerPersisted = (Customer) customerService.createOrUpdate(customer);
			response.setData(customerPersisted);
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	private void validateUpdateCustomer(Customer customer, BindingResult result) {
		if (customer.getId() == null) {
			result.addError(new ObjectError("Customer", "Id não informado"));
			return;
		}
		if (customer.getName() == null) {
			result.addError(new ObjectError("Customer", "Nome não informado"));
			return;
		}
		if (customer.getReputation() == null) {
			result.addError(new ObjectError("Customer", "Reputação não informada"));
			return;
		}
		if (customer.getGender() == null) {
			result.addError(new ObjectError("Customer", "Sexo não informado"));
			return;
		}
		if (customer.getTelephone().get(0) == null) {
			result.addError(new ObjectError("Customer", "Celular não informado"));
			return;
		}
	}
	
	@GetMapping(value = "{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<Customer>> findById(@PathVariable("id") String id) {
		Response<Customer> response = new Response<Customer>();
		Customer customer = customerService.findById(id);
		if (customer == null) {
			response.getErrors().add("Register not found id:" + id);
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(customer);
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<String>> delete(@PathVariable("id") String id) {
		Response<String> response = new Response<String>();
		Customer customer = customerService.findById(id);
		if (customer == null) {
			response.getErrors().add("Register not found id:" + id);
			return ResponseEntity.badRequest().body(response);
		}
		customerService.delete(id);
		return ResponseEntity.ok(new Response<String>());
	}
	
	@GetMapping(value = "{page}/{count}")
	@PreAuthorize("hasAnyRole('ADMIN')")
    public  ResponseEntity<Response<Page<Customer>>> findAll(HttpServletRequest request, 
    		@PathVariable int page, @PathVariable int count) {
		Response<Page<Customer>> response = new Response<Page<Customer>>();
		Page<Customer> customers = customerService.list(page, count);
		response.setData(customers);
		return ResponseEntity.ok(response);
    }
	
	@GetMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
    public  ResponseEntity<Response<Iterable<Customer>>> findAll(HttpServletRequest request) {
		Response<Iterable<Customer>> response = new Response<>();
		Iterable<Customer> customers = customerService.findAll();
		response.setData(customers);
		return ResponseEntity.ok(response);
    }
	
	@GetMapping(value = "{page}/{count}/{name}/{reputation}/{telephone}/{cpf}/"
			+ "{startDt}/{endDt}/{startPeriod}/{endPeriod}")
	@PreAuthorize("hasAnyRole('ADMIN')")
    public  ResponseEntity<Response<Page<Customer>>> findByParams(HttpServletRequest request, 
    		 							@PathVariable int page, 
    		 							@PathVariable int count,
    		 							@PathVariable String name,
    		 							@PathVariable String reputation,
    		 							@PathVariable String telephone,
    		 							@PathVariable String cpf,
    		 							@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDt,
    		 							@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDt,
    		 							@PathVariable int startPeriod,
    		 							@PathVariable int endPeriod) {
		
		name = name.trim().equals("uninformed") ? "" : name.trim();
		reputation = reputation.equals("uninformed") ? "" : reputation;
		telephone = telephone.equals("uninformed") ? "" : telephone;
		cpf = cpf.equals("uninformed") ? "" : cpf;

		Response<Page<Customer>> response = new Response<Page<Customer>>();
		Page<Customer> customers = customerService.findByParameters(page, count, name, reputation, telephone, 
				startDt, endDt, cpf, startPeriod, endPeriod);
		response.setData(customers);
		return ResponseEntity.ok(response);
    }

}
