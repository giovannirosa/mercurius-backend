package com.grosa.mercurius.api.enums;

public enum ReputationEnum {
	
	Boa,
	Média,
	Ruim;

	public static ReputationEnum getStatus(String status) {
		switch(status) {
		case "Boa": return Boa;
		case "Média": return Média;
		case "Ruim": return Ruim;
		default: return Boa;
		}
	}
}
