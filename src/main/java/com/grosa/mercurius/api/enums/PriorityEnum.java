package com.grosa.mercurius.api.enums;

public enum PriorityEnum {
	High,
	Normal,
	Low
}
