package com.grosa.mercurius.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.grosa.mercurius.api.entity.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findByEmail(String email);

}
