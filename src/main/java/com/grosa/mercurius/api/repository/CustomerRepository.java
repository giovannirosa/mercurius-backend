package com.grosa.mercurius.api.repository;

import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.grosa.mercurius.api.entity.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {
	
	Page<Customer> findByIdOrderByName(Pageable pages, String id);
	
	Page<Customer> findByNameIgnoreCaseContainingAndReputationIgnoreCaseContainingAndTelephoneIgnoreCaseContainingAndBirthdayBetweenAndCpfIgnoreCaseContainingAndPeriodBetweenOrderByName(
			String name,String reputation,String telephone,Date startDt,Date endDt,String cpf,Integer startPeriod,Integer endPeriod,Pageable pages);

	Page<Customer> findAllOrderByName(Pageable pages);

}
