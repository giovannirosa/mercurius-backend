package com.grosa.mercurius.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.grosa.mercurius.api.entity.ChangeStatus;

public interface ChangeStatusRepository extends MongoRepository<ChangeStatus, String> {

	Iterable<ChangeStatus> findByTicketIdOrderByDateChangeStatusDesc(String ticketId);
}
