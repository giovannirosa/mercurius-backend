package com.grosa.mercurius.api.entity;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.grosa.mercurius.api.enums.GenderEnum;
import com.grosa.mercurius.api.enums.ReputationEnum;

@Document
public class Customer {
	
	@Id
	private String id;
	
	@Indexed(unique = true)
	@NotBlank(message = "Name required")
	private String name;
	
	@NotBlank(message = "Reputation required")
	private ReputationEnum reputation;
	
	@NotBlank(message = "Gender required")
	private GenderEnum gender;
	
	private List<String> telephone;
	
	private List<String> email;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;

	private String cpf;
	
	private Integer period;
	
	private String notes;
	
	private Date dateEntered;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public ReputationEnum getReputation() {
		return reputation;
	}

	public void setReputation(ReputationEnum reputation) {
		this.reputation = reputation;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getDateEntered() {
		return dateEntered;
	}

	public void setDateEntered(Date dateEntered) {
		this.dateEntered = dateEntered;
	}

	public List<String> getEmail() {
		return email;
	}

	public void setEmail(List<String> email) {
		this.email = email;
	}

	public List<String> getTelephone() {
		return telephone;
	}

	public void setTelephone(List<String> telephone) {
		this.telephone = telephone;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Customer))
			return false;
		return this.name.equals(((Customer)obj).getName());
	}
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
}
